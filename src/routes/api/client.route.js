'use strict'

const express = require('express')
const router = express.Router()
const clientController = require('../../controllers/client.controller')
const auth = require('../../middlewares/authorization')

router.post('/client', auth(), clientController.create)
router.get('/client', auth(), clientController.find)
router.get('/client/:id', auth(), clientController.findOneById)
router.put('/client/:id', auth(), clientController.updateOne)
router.delete('/client/:id', auth(), clientController.deleteOne)

module.exports = router