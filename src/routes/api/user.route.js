'use strict'

const express = require('express')
const router = express.Router()
const userController = require('../../controllers/user.controller')
const auth = require('../../middlewares/authorization')

router.get('/user', auth(), userController.find)
router.get('/user/:id', auth(), userController.findOne)

module.exports = router