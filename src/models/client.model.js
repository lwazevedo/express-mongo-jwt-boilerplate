'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const tipoPessoa = ['F', 'J']
const Address = require('./address.model')
const Contact = require('./contact.model')


const clientSchema = new Schema({
    nome: {
        type: String,
        required: function () { return this.tipo_pessoa === 'F' },
        lowercase: true,
        hidden: function () { return this.tipo_pessoa === 'J' }
    },
    nome_fantasia: {
        type: String,
        required: function () { return this.tipo_pessoa === 'J' },
        lowercase: true,
        hidden: true
    },
    razao_social: {
        type: String,
        required: function () { return this.tipo_pessoa === 'J' },
        lowercase: true
    },
    ie: {
        type: Number,
    },
    im: {
        type: Number,
    },
    tipo_pessoa: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 1,
        enum: tipoPessoa
    },
    documento: {
        type: String,
        required: true
    },
    address: [Address],
    contact: [Contact],
    additional_data: {
        type: String,
        lowercase: true
    },
    note: {
        type: String,
        lowercase: true
    },
    observation_request: {
        type: String,
        lowercase: true
    }

}, {
        timestamps: true
    })


clientSchema.methods.toJSON = function () {
    let obj = this.toObject()
    delete obj.__v
    obj.tipo_pessoa === 'J' ? delete obj.nome : (delete obj.nome_fantasia, delete obj.razao_social, delete obj.ie, delete obj.im)
    return obj
}


module.exports = mongoose.model('Client', clientSchema)   