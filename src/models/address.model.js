const mongoose = require('mongoose')
const Schema = mongoose.Schema

const tiposEnd = ['Principal', 'Entrega']

const addressSchema = new Schema({
    address: {
        type: String,
        required: true,
        lowercase: true
    },
    number: {
        type: Number,
        required: true,
        lowercase: true
    },
    state: {
        type: String,
        required: true,
        lowercase: true
    },
    state_initials: {
        type: String,
        required: true,
        lowercase: true,
        maxlength: 2,
        minlength: 2
    },
    city: {
        type: String,
        required: true,
        lowercase: true
    },
    tiposEnd: {
        type: String,
        required: true,
        enum: tiposEnd
    }  
})

module.exports = addressSchema;  