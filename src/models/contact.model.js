const mongoose = require('mongoose')
const Schema = mongoose.Schema

const tipos = ['Telefone', 'Email', 'Email_NFE']

const contactSchema = new Schema({
    name: {
        type: String,
        required: true,
        lowercase: true
    },
    info_contact: {
        type: String,
        required: true,
        lowercase: true
    },
    date_birthday: {
        type: String,
        required: true,
        lowercase: true
    },
    telefone: {
        type: String,
        required: true,
        lowercase: true
    },
    tipo: {
        type: String,
        required: true,
        lowercase: true,
        enum: tipos
    }  
})

module.exports = contactSchema;  