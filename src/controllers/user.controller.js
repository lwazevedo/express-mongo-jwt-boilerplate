"use strict";

const User = require("../models/user.model");
const config = require("../config");
const httpStatus = require("http-status");

exports.findOne = async (req, res, next) => {
  try {
    const user = (await User.findOne({ _id: req.params.id })) || {};
    res.status(httpStatus.OK);
    res.send(user);
  } catch (error) {
    console.log(error);
    next(error);
  }
};

exports.find = async (req, res, next) => {
  try {
    const users = (await User.find({})) || [];
    res.status(httpStatus.OK);
    res.send(users);
  } catch (error) {
    console.log(error);
    next(error);
  }
};
