"use strict";

const Client = require("../models/client.model");
const httpStatus = require("http-status");

exports.create = async (req, res, next) => {
    try {
        const client = new Client(req.body)
        const savedClient = await client.save()
        res.status(httpStatus.CREATED)
        res.send(savedClient)
    } catch (error) {
        return next(error)
    }
};

exports.find = async (req, res, next) => {
    try {
        const clients = (await Client.find({}).sort()) || [];
        res.status(httpStatus.OK);
        res.send(clients);
    } catch (error) {
        console.log(error);
        next(error);
    }
};

exports.findOneById = async (req, res, next) => {
    try {
        const clients = (await Client.findOne({_id:req.params.id}).sort()) || [];
        res.status(httpStatus.OK);
        res.send(clients);
    } catch (error) {
        console.log(error);
        next(error);
    }
};


exports.updateOne = async(req, res, next) => {
    try {
        const clients = (await Client.updateOne({_id:req.params.id},req.body)) || [];
        res.status(httpStatus.OK);
        res.send(clients);
    } catch (error) {
        console.log(error);
        next(error);
    }
}

exports.deleteOne = async(req, res, next) => {
    try {
        const clients = (await Client.deleteOne({_id:req.params.id})) || [];
        res.status(httpStatus.OK);
        res.send(clients);
    } catch (error) {
        console.log(error);
        next(error);
    }
}